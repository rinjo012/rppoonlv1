﻿using System;

namespace RPPOON_LV1
{
    class Program
    {
        class Note
        {
            private string NoteAuthor;
            private string NoteText;
            private int NotePriority;

            public Note()
            {
                this.NoteAuthor = "Leon Rincic";
                this.NoteText = "Default";
                this.NotePriority = 5;
            }

            public Note(string NoteAuthor,string NoteText,int NotePriority)
            {
                this.NoteAuthor = NoteAuthor;
                this.NoteText = NoteText;
                this.NotePriority = NotePriority;
            }
            public Note(Note CNote)
            {
                this.NoteAuthor = CNote.NoteAuthor;
                this.NoteText = CNote.NoteText;
                this.NotePriority = CNote.NotePriority;
            }



            public string getNoteAuthor() { return this.NoteAuthor; }
            public string getNoteText() { return this.NoteText; }
            public int getNotePriority() { return this.NotePriority; }
            public void setNoteText(string NoteText) { this.NoteText = NoteText; }
            public void setNotePriority(int NotePriority) { this.NotePriority = NotePriority; }
        }

        static void Main(string[] args)
        {
            Note First;
            First = new Note();
            Console.WriteLine(First.getNoteAuthor());
            Console.WriteLine(First.getNoteText());
            Note Second;
            Second = new Note("Ivan Horvat", "Default2", 3);
            Console.WriteLine(Second.getNoteAuthor());
            Console.WriteLine(Second.getNoteText());
            Note Third;
            Third = new Note(Second);
            Console.WriteLine(Third.getNoteAuthor());
            Console.WriteLine(Third.getNoteText());
        }
    }
}
